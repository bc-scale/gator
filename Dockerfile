FROM rockylinux/rockylinux:8 as gator-grabber

LABEL maintainer="Matthew Mix <matthew.w.mix@gmail.com> and Andrew Kozak <andrewkozak11@gmail.com>"

ARG GATOR_VERSION=v3.8.0
ARG UID=1000
ARG GID=1000

RUN dnf update -y && \
    dnf install wget -y && \
    dnf clean all

# Add gator (for gatekeeper unit testing)
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN wget -qO- https://github.com/open-policy-agent/gatekeeper/releases/download/${GATOR_VERSION}/gator-${GATOR_VERSION}-linux-amd64.tar.gz | tar -xz -C /bin/ && \
    dnf remove wget -y && \
    chmod 755 /bin/gator

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN groupadd -g "${GID}" gator || groupmod -n gator "$(getent group ${GID} | cut -d: -f1)" && \
    useradd -l --shell /bin/bash -u "${UID}" -g "${GID}" -m gator && \
    chmod 755 /bin/gator

USER gator

ENTRYPOINT ["/bin/gator"]
