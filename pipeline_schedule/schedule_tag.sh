#!/usr/bin/env bash

curl --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
--form description="Weekly Build" --form ref="${TAG}" --form cron="0 8 * * 5" --form cron_timezone="EST" \
--form active="true" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/pipeline_schedules" 
